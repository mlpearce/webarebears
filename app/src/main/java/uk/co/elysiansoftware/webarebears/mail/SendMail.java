package uk.co.elysiansoftware.webarebears.mail;

import android.util.Log;

import com.sun.mail.smtp.SMTPAddressFailedException;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Class to actually send an email.
 * <p>
 * Created by mlp on 03/01/18.
 */
public class SendMail extends javax.mail.Authenticator {

	private static final String TAG = "SendMail";

	private final String smtpServer;
	private final int smtpPort;
	private final String smtpUser;
	private final String smtpPassword;
	private final String fromAddress;

	private Session session;

	SendMail(String smtpServer, int smtpPort, String smtpUser, String smtpPassword, String fromAddress) {
		this.smtpServer = smtpServer;
		this.smtpPort = smtpPort;
		this.smtpUser = smtpUser;
		this.smtpPassword = smtpPassword;
		this.fromAddress = fromAddress;
	}

	boolean isInitialised() {
		return smtpServer != null && !smtpServer.equals("")
				&& smtpUser != null && !smtpUser.equals("")
				&& smtpPassword != null && !smtpPassword.equals("");
	}

	private Session initialiseSession() {
		Properties props = setProperties();
		return Session.getInstance(props, this);
	}

	private Properties setProperties() {
		Properties props = new Properties();

		props.put("mail.smtp.host", smtpServer);
		props.put("mail.smtp.auth", "true");

		props.put("mail.smtp.port", smtpPort);
		props.put("mail.smtp.socketFactory.port", smtpPort);
		if (smtpPort == 465) {
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "false");
		} else if (smtpPort == 587) {
			props.put("mail.smtp.starttls.enable", true);
		}

		return props;
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(smtpUser, smtpPassword);
	}

	boolean send(String address, String subject, String plainText, String htmlText, String photoFile) {
		boolean ret = false;

		if (photoFile == null) {
			Log.w(TAG, "No photo file for user - skipping send");
			ret = true;
		} else {
			if (session == null) {
				session = initialiseSession();
			}

			try {
				Multipart textParts = new MimeMultipart("alternative");

				MimeMessage msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(fromAddress));
				msg.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(address)});
				msg.setSubject(subject);
				msg.setSentDate(new Date());

				// Set the plain text message body
				BodyPart textBodyPart = new MimeBodyPart();
				textBodyPart.setContent(plainText, "text/plain; charset=utf-8");
				textParts.addBodyPart(textBodyPart);

				// Set the HTML message body
				BodyPart htmlBodyPart = new MimeBodyPart();
				htmlBodyPart.setContent(htmlText, "text/html; charset=utf-8");
				textParts.addBodyPart(htmlBodyPart);

				MimeBodyPart emailText = new MimeBodyPart();
				emailText.setContent(textParts);
				Multipart emailParts = new MimeMultipart("mixed");
				emailParts.addBodyPart(emailText);

				// Add the photo attachment
				BodyPart attachment = buildAttachment(photoFile);
				if (attachment != null) {
					emailParts.addBodyPart(attachment);
				}

				// Add the content to the message
				msg.setContent(emailParts);

				// Send the message
				Transport.send(msg);

				ret = true;
			} catch (AddressException e) {
				Log.e(TAG, "Internet address exception: " + e.getMessage());
				// Return true - email address badly formatted
				ret = true;
			} catch (MessagingException e) {
				Log.e(TAG, "Messaging exception: " + e.getMessage(), e);
				if (e.getNextException() instanceof SMTPAddressFailedException) {
					Log.e(TAG, "Invalid email address");
					ret = true;
				}
			}
		}

		return ret;
	}

	private BodyPart buildAttachment(String filename) throws MessagingException {
		BodyPart attachmentPart = new MimeBodyPart();

		try {
			File attachFile = new File(filename);
			DataSource source = new FileDataSource(attachFile);
			attachmentPart.setDataHandler(new DataHandler(source));
			attachmentPart.setFileName(attachFile.getName());
		} catch (MessagingException e) {
			Log.e(TAG, "Exception caught building attachment: " + e.getMessage());
			attachmentPart = null;
		}

		return attachmentPart;
	}

}
