package uk.co.elysiansoftware.webarebears;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import uk.co.elysiansoftware.webarebears.mail.SendMailJobService;

public class MainActivity extends SingleFragmentActivity {

	private static final String TAG = MainActivity.class.getSimpleName();

	// How often the job should repeat
	private static final long JOB_SCHEDULE_PERIOD = 5 * 60 * 1000;
	// The (fixed) job ID to use for the scheduled mail send
	private static final int JOB_ID = 1;

	private JobScheduler mJobScheduler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Load the job scheduler
		mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
		JobInfo j = mJobScheduler.getPendingJob(JOB_ID);
		if (mJobScheduler.getPendingJob(JOB_ID) == null) {
			// Add a repeating job to it
			JobInfo jobInfo = new JobInfo.Builder(JOB_ID, new ComponentName(getPackageName(), SendMailJobService.class.getName()))
					.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
					.setPeriodic(JOB_SCHEDULE_PERIOD)
					.build();
			if (mJobScheduler.schedule(jobInfo) == JobScheduler.RESULT_FAILURE) {
				Log.e(TAG, "Could not schedule mail send job");
			} else {
				Log.d(TAG, "Mail send job scheduled");
			}
		}

		// Run the remaining create method
		super.onCreate(savedInstanceState);
	}

	@Override
	protected Fragment createFragment() {
		return new StartFragment();
	}
}
