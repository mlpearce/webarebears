package uk.co.elysiansoftware.webarebears;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Fragment implementing the preferences dialog.
 * <p>
 * Created by mlp on 05/01/18.
 */
public class SettingsFragment extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences
		addPreferencesFromResource(R.xml.preferences);
	}
}
