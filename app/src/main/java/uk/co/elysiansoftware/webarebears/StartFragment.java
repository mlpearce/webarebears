package uk.co.elysiansoftware.webarebears;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Starting screen - just displays a "Start" button, leading to the main
 * details entry form.
 * <p>
 * Also implements permissions checks.
 * <p>
 * Created by mlp on 15/12/17.
 */
public class StartFragment extends Fragment {

	private static final int REQUEST_CAMERA_PERMISSION = 200;

	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_start, parent, false);

		Button startButton = v.findViewById(R.id.start_button);
		startButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), FormActivity.class);
				startActivity(i);
			}
		});

		ImageButton settingsButton = v.findViewById(R.id.settings_button);
		settingsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), SettingsActivity.class);
				startActivity(i);
			}
		});

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		checkPermissions();
	}

	private void checkPermissions() {
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
				|| ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(getActivity(),
					new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA },
					REQUEST_CAMERA_PERMISSION);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (requestCode == REQUEST_CAMERA_PERMISSION) {
			for (int i = 0; i < grantResults.length; i ++) {
				if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
					Toast.makeText(getActivity().getApplicationContext(),
							"Sorry, you cannot use this app without granting permission", Toast.LENGTH_LONG).show();
				}
			}
		}
	}
}
