package uk.co.elysiansoftware.webarebears.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Data access object for the User table.
 * <p>
 * Created by mlp on 18/12/17.
 */
@Dao
public interface UserDao {

	@Insert
	long addUser(User user);

	@Update
	void updateUser(User user);

	@Query("UPDATE User SET pictureFile = :filename WHERE uid = :uid")
	void updatePictureFile(long uid, String filename);

	@Query("SELECT * FROM User WHERE uid = :uid")
	User getUserById(long uid);

	@Query("SELECT uid FROM User WHERE sentUser = 0 AND pictureFile IS NOT NULL")
	List<Integer> getUnsentUsers();

}
