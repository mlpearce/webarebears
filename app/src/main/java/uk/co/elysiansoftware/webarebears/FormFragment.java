package uk.co.elysiansoftware.webarebears;

import android.app.Fragment;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.co.elysiansoftware.webarebears.data.User;
import uk.co.elysiansoftware.webarebears.data.UserTask;
import uk.co.elysiansoftware.webarebears.mail.SendMailJobService;

import static android.app.Activity.RESULT_OK;

/**
 * Fragment managing user details form.
 * <p>
 * Created by mlp on 15/12/17.
 */
public class FormFragment extends Fragment {

	public static final String WBB_FILE_PREFIX = "WBB_";
	public static final String JPEG_EXTENSION = ".jpg";

	private static final String TAG = "WBB_FormFragment";
	private static final String INSERT_INTENT_CALLBACK = "WBB_InsertCallback";

	static final int REQUEST_TAKE_PHOTO = 1;

	private EditText mNameText;
	private EditText mEmailText;
	private String mCurrentPhotoPath;

	private Button mSaveButton;

	private User mUser;

	private ProgressBar mProgressBar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		mUser = new User();

		View v = inflater.inflate(R.layout.fragment_form, parent, false);

		mNameText = v.findViewById(R.id.nameText);
		mNameText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				mUser.setName(s.toString());
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		mEmailText = v.findViewById(R.id.emailText);
		mEmailText.addTextChangedListener(new TextValidator(mEmailText) {
			@Override
			public void validate(TextView textView, String text) {
				if (text == null || text.equals("")) {
					textView.setError(getString(R.string.no_email_error));
				} else {
					if (textView.getError() != null) {
						textView.setError(null);
					}
					mUser.setEmailAddress(text);
				}
			}
		});

		mProgressBar = v.findViewById(R.id.progress);

		mSaveButton = v.findViewById(R.id.selfieButton);
		mSaveButton.setActivated(true);
		mSaveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (validate()) {
					mSaveButton.setActivated(false);
					dispatchTakePictureIntent();
				}
			}
		});

		return v;
	}

	private boolean validate() {
		boolean inFault = false;
		if (isBlank(mEmailText)) {
			mEmailText.setError(getString(R.string.no_email_error));
			inFault = true;
		}

		return !inFault;
	}

	private boolean isBlank(EditText field) {
		return field.getText() == null || field.getText().toString().trim().equals("");
	}

	@Override
	public void onResume() {
		Log.d(TAG, "onStart");
		super.onResume();
		getActivity().registerReceiver(receiver, new IntentFilter(INSERT_INTENT_CALLBACK));
	}

	@Override
	public void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		getActivity().unregisterReceiver(receiver);
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.UK).format(new Date());
		String imageFileName = WBB_FILE_PREFIX + timeStamp;
		File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File image = new File(storageDir, imageFileName + JPEG_EXTENSION);
		if (!image.createNewFile()) {
			image = File.createTempFile(
					imageFileName,  /* prefix */
					JPEG_EXTENSION,         /* suffix */
					storageDir      /* directory */
			);
		}

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		return image;
	}

	/**
	 * Dispatch to the camera app, allowing the user to take a picture.
	 */
	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
				Log.e(TAG, "Could not create image file: " + ex.getMessage());
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				Uri photoURI = FileProvider.getUriForFile(getActivity().getApplicationContext(),
						"uk.co.elysiansoftware",
						photoFile);
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		}
	}

	/**
	 * Catch return from activity (most likely the picture taking).
	 * @param requestCode the code used to request the activity.
	 * @param resultCode the activity's result.
	 * @param data the data returned from the activity.
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_TAKE_PHOTO) {
			if (resultCode == RESULT_OK) {
				// Picture has been taken - store in the user record
				mUser.setPictureFile(mCurrentPhotoPath);
				// Turn on the progress spinner
				mProgressBar.setVisibility(View.VISIBLE);
				// Store the user data
				new UserTask(getActivity().getApplicationContext(), UserTask.UserTaskAction.INSERT, INSERT_INTENT_CALLBACK)
						.execute(mUser);
			}
		} else {
			// Something funny going on - finish the activity, return to the start screen
			getActivity().finish();
		}
	}

	/**
	 * Broadcast receiver, catches return from UserTask (inserting data
	 * to DB). Clears progress bar, returns to start screen.
	 */
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Long uid = intent.getLongExtra(UserTask.INSERT_RESPONSE, 0);
			Log.d(TAG, "RESPONSE = " + uid);

			// Fetch the job schedule, and add a single-run job to send the latest mail
			JobScheduler mJobScheduler = (JobScheduler) getActivity().getSystemService(Context.JOB_SCHEDULER_SERVICE);
			// Add a job to it
			JobInfo jobInfo = new JobInfo.Builder(uid.intValue(), new ComponentName(getActivity().getPackageName(), SendMailJobService.class.getName()))
					.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
					.build();
			if (mJobScheduler.schedule(jobInfo) == JobScheduler.RESULT_FAILURE) {
				Log.e(TAG, "Could not schedule single mail send job");
			} else {
				Log.d(TAG, "Mail send job scheduled for UID " + uid);
			}

			// clear the progress indicator
			if (mProgressBar != null) {
				mProgressBar.setVisibility(View.GONE);
			}

			getActivity().finish();
		}
	};

}
