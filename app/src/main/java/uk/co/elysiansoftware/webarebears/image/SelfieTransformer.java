package uk.co.elysiansoftware.webarebears.image;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.jpeg.JpegDirectory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * Utility class for adding watermarks to selfie images.
 * <p>
 * Created by mlp on 18/01/18.
 */
public class SelfieTransformer {

	private static final String TAG = "WBB_SelfieTransformer";

	private static final String SELFIE_OVERLAY = "selfie_overlay.png";

	public static boolean addLogosToSelfie(AssetManager assetManager, File inFile, File outFile) {
		boolean ret = false;

		try {
			// Read the input file into a bitmap, applying transformations as necessary
			Bitmap srcBitmap = BitmapFactory.decodeFile(inFile.getAbsolutePath());

			// Create a destination bitmap to hold the final image
			Bitmap destBitmap = Bitmap.createBitmap(srcBitmap.getWidth(), srcBitmap.getHeight(), srcBitmap.getConfig());
			// Create a canvas using the destination bitmap
			Canvas canvas = new Canvas(destBitmap);

			// Add the incoming bitmap, including any transformations required
			ImageInformation info = readImageInformation(inFile);
			canvas.drawBitmap(srcBitmap, getExifTransformation(info), null);

			// Add the overlay - fits over the whole image
			Bitmap overlay = readBitmapFromAsset(assetManager, SELFIE_OVERLAY);
			canvas.drawBitmap(overlay, 0, 0, null);

			// Write out the destination bitmap to the output file
			ret = writeBitmapToFile(destBitmap, outFile);
		} catch (IOException e) {
			Log.e(TAG, "Caught IO exception reading image details: " + e.getMessage());
		} catch (MetadataException e) {
			Log.e(TAG, "Caught metadata exception reading image details: " + e.getMessage());
		} catch (ImageProcessingException e) {
			Log.e(TAG, "Image processing exception: " + e.getMessage());
		}

		return ret;
	}

	private static boolean writeBitmapToFile(Bitmap bitmap, File outFile) {
		boolean ret = false;
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(outFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.flush();
			ret = true;
		} catch (IOException e) {
			Log.e(TAG, "Caught IOException writing modified bitmap to file: " + e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					Log.e(TAG, "IOException caught closing output file: " + e.getMessage());
				}
			}
		}

		return ret;
	}

	private static Bitmap readBitmapFromAsset(AssetManager assetManager, String assetFile) throws IOException {
		InputStream is = assetManager.open(assetFile);
		Bitmap bm = BitmapFactory.decodeStream(is);
		is.close();

		return bm;
	}

	private static ImageInformation readImageInformation(File imageFile) throws IOException, MetadataException, ImageProcessingException {
		Metadata metadata = ImageMetadataReader.readMetadata(imageFile);
		Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
		JpegDirectory jpegDirectory = metadata.getFirstDirectoryOfType(JpegDirectory.class);

		int orientation = 1;
		try {
			orientation = directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
		} catch (MetadataException me) {
			System.err.println("Could not get orientation");
		}
		int width = jpegDirectory.getImageWidth();
		int height = jpegDirectory.getImageHeight();

		return new ImageInformation(orientation, width, height);
	}

	// Look at http://chunter.tistory.com/143 for information
	private static Matrix getExifTransformation(ImageInformation info) {
		Matrix matrix = new Matrix();

		Log.d(TAG, "Image orientation: " + info.orientation);

		// Images only likely to be at orientation 1-4 (landscape).
		// Orientations 5-8 are all portrait.
		switch (info.orientation) {
			case 1:
				break;
			case 2: // Flip X
				matrix.setTranslate(-info.width, 0f);
				matrix.postScale(-1.0f, 1.0f);
				break;
			case 3: // PI rotation
				matrix.setRotate(180);
				matrix.postTranslate(info.width, info.height);
				break;
			case 4: // Flip Y
				matrix.setTranslate(0f, -info.height);
				matrix.postScale(1.0f, -1f);
				break;
			case 5: // - PI/2 and Flip X
				matrix.postRotate(-90);
				matrix.postScale(-1.0f, 1.0f);
				break;
			case 6: // -PI/2 and -width
				matrix.setTranslate(info.height, 0f);
				matrix.postRotate(90);
//				t.translate(info.height, 0);
//				t.rotate(Math.PI / 2);
				break;
			case 7: // PI/2 and Flip
				matrix.postScale(-1.0f, 1.0f);
				matrix.postRotate(270);
//				t.scale(-1.0, 1.0);
//				t.translate(-info.height, 0);
//				t.translate(0, info.width);
//				t.rotate(  3 * Math.PI / 2);
				break;
			case 8: // PI / 2
				matrix.postRotate(270);
//				t.translate(0, info.width);
//				t.rotate(  3 * Math.PI / 2);
				break;
		}

		return matrix;
	}

	// Inner class containing image information
	private static class ImageInformation {
		final int orientation;
		final int width;
		final int height;

		ImageInformation(int orientation, int width, int height) {
			this.orientation = orientation;
			this.width = width;
			this.height = height;
		}

		@Override
		public String toString() {
			return String.format(Locale.UK, "%dx%d,%d", this.width, this.height, this.orientation);
		}
	}


}
