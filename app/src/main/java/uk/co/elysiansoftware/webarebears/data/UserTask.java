package uk.co.elysiansoftware.webarebears.data;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;

import uk.co.elysiansoftware.webarebears.image.SelfieTransformer;

/**
 * Asynchronous task to save user in DB.
 * <p>
 * Created by mlp on 18/12/17.
 */
public class UserTask extends AsyncTask<User, Void, Long> {

	private static final String TAG = "WBB_InsertUserTask";

	public static final String INSERT_RESPONSE = "WBB_UID";

	public enum UserTaskAction {
		INSERT,
		UPDATE,
		UPDATE_PICTUREFILE
	}

	private final WeakReference<Context> mContext;
	private final UserTaskAction mAction;
	private final String mIntentCallback;

	public UserTask(Context context, UserTaskAction action, String intentCallback) {
		this.mContext = new WeakReference<>(context);
		this.mAction = action;
		this.mIntentCallback = intentCallback;
	}

	@Override
	protected Long doInBackground(User... users) {
		AppDatabase db = AppDatabase.getAppDatabase(mContext.get());

		long rowId = 0;
		if (mAction == UserTaskAction.INSERT) {
			// TEST: users[0].setPictureFile("/storage/emulated/0/DCIM/WBB_20180124_095255.jpg");
			if (users[0].getPictureFile() != null && !users[0].getPictureFile().equals("")) {
				users[0].setPictureFile(addLogosToImage(users[0].getPictureFile()));
			}
			if (users[0].getUid() != null) {
				rowId = users[0].getUid();
				db.userDao().updateUser(users[0]);
				Log.d(TAG, "Updated user " + rowId);
			} else {
				rowId = db.userDao().addUser(users[0]);
				users[0].setUid(rowId);
				Log.d(TAG, "Added user with rowID " + rowId);
			}
		} else if (mAction == UserTaskAction.UPDATE) {
			rowId = users[0].getUid();
			db.userDao().updateUser(users[0]);
			Log.d(TAG, "Updated user " + rowId);
		} else if (mAction == UserTaskAction.UPDATE_PICTUREFILE) {
			rowId = users[0].getUid();
			db.userDao().updatePictureFile(rowId, users[0].getPictureFile());
			Log.d(TAG, "Updated user picture file - " + rowId + " -> " + users[0].getPictureFile());
		}

		return rowId;
	}

	private String addLogosToImage(String imageFile) {
		final String ret;
		File inFile = new File(imageFile);
		File outFile = new File(imageFile.replaceFirst("\\.jpg", "t.jpg"));
		if (SelfieTransformer.addLogosToSelfie(mContext.get().getAssets(), inFile, outFile)) {
			ret = outFile.getAbsolutePath();
		} else {
			ret = imageFile;
		}
		return ret;
	}

	@Override
	protected void onPostExecute(Long uid) {
		super.onPostExecute(uid);
		Intent i = new Intent(mIntentCallback);
		i.putExtra(INSERT_RESPONSE, uid);
		mContext.get().sendBroadcast(i);
	}
}
