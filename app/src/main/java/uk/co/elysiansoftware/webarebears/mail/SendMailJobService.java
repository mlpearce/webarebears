package uk.co.elysiansoftware.webarebears.mail;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.Iterator;
import java.util.List;

import uk.co.elysiansoftware.webarebears.R;
import uk.co.elysiansoftware.webarebears.SettingsActivity;
import uk.co.elysiansoftware.webarebears.data.AppDatabase;
import uk.co.elysiansoftware.webarebears.data.User;

/**
 * Job service implementation to send any outstanding emails.
 * <p>
 * Created by mlp on 04/01/18.
 */
public class SendMailJobService extends JobService {

	private static final String TAG = SendMailJobService.class.getSimpleName();

	private static final int SEND_EMAILS = 1;
	private static final int ABORT_EMAILS = 2;

	private Handler mHandler = new Handler(new Handler.Callback() {
		SendMailThread sendMailThread;

		@Override
		public boolean handleMessage(Message msg) {
			boolean ret = false;

			if (msg.what == SEND_EMAILS) {
				if (sendMailThread == null || !sendMailThread.isAlive()) {
					sendMailThread = new SendMailThread((JobParameters) msg.obj);
					sendMailThread.start();
				}

				ret = true;
			} else if (msg.what == ABORT_EMAILS) {
				// Stop running
				if (sendMailThread.isAlive()) {
					sendMailThread.running = false;
				}
			}

			return ret;
		}
	});

	@Override
	public boolean onStartJob(final JobParameters params) {
		Log.d(TAG, "Starting mail send job");
		mHandler.sendMessage(Message.obtain(mHandler, 1, params));
		return true;
	}

	@Override
	public boolean onStopJob(JobParameters params) {
		mHandler.sendEmptyMessage(ABORT_EMAILS);
		mHandler.removeMessages(SEND_EMAILS);
		return false;
	}

	class SendMailThread extends Thread {

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		boolean running = true;
		JobParameters parameters;

		SendMailThread(JobParameters params) {
			this.parameters = params;
			setName(parameters.getJobId() + "_SendMailThread");
		}

		@Override
		public void run() {
			List<Integer> uids = AppDatabase.getAppDatabase(getApplicationContext()).userDao().getUnsentUsers();
			if (uids != null && !uids.isEmpty()) {
				Log.d(TAG, "Found " + uids.size() + " unsent emails");
				SendMail sendMail = initialiseSendMail();
				if (sendMail.isInitialised()) {
					for (Iterator<Integer> it = uids.iterator(); it.hasNext() && running; ) {
						int uid = it.next();
						sendMailToUser(sendMail, uid);
					}
				} else {
					Log.w(TAG, "SendMail could not be initialised - missing properties!");
				}
			}

			// Indicate job has finished - never needs reschedule, since
			// next job will build all mails if this fails to do so.
			jobFinished(parameters, false);
		}

		private SendMail initialiseSendMail() {
			String smtpServer = preferences.getString(SettingsActivity.SMTP_ADDRESS_PREF, null);
			int smtpPort = Integer.valueOf(preferences.getString(SettingsActivity.SMTP_PORT_PREF, "0"));
			String smtpUser = preferences.getString(SettingsActivity.SMTP_USER_PREF, null);
			String smtpPassword = preferences.getString(SettingsActivity.SMTP_PASSWORD_PREF, null);
			String fromAddress = preferences.getString(SettingsActivity.FROM_ADDRESS_PREF, getApplicationContext().getString(R.string.from_address));

			return new SendMail(smtpServer, smtpPort, smtpUser, smtpPassword, fromAddress);
		}

		private void sendMailToUser(SendMail sendMail, long uid) {
			// Look up the user object
			User user = findUser(uid);

			if (user != null) {
				boolean sent;

				String userSubject = getApplicationContext().getString(R.string.user_subject);
				String userPlainEmailContent = getApplicationContext().getString(R.string.user_bodytext);
				String userHtmlEmailContent = getApplicationContext().getString(R.string.user_htmltext);

				userPlainEmailContent = userPlainEmailContent.replace("%username%", user.getName());
				userHtmlEmailContent = userHtmlEmailContent.replace("%username%", TextUtils.htmlEncode(user.getName()));

				// Send mail to the user
				if (sent = sendMail.send(user.getEmailAddress(), userSubject, userPlainEmailContent, userHtmlEmailContent, user.getPictureFile())) {
					// Update the UI progress bar
					user.setSentUser(1);
				}

				if (sent) {
					updateUser(user);
				}
			}
		}

		private User findUser(long userId) {
			return AppDatabase.getAppDatabase(getApplicationContext()).userDao().getUserById(userId);
		}

		private void updateUser(User user) {
			Log.d(TAG, "Updating user " + user.getUid() + " with sentUser=" + user.getSentUser());
			AppDatabase.getAppDatabase(getApplicationContext()).userDao().updateUser(user);
		}
	}
}
