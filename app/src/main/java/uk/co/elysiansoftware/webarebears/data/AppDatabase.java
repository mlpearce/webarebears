package uk.co.elysiansoftware.webarebears.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Database handler class, with singleton access to DB.
 * <p>
 * Created by mlp on 18/12/17.
 */
@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

	private static final String DB_NAME = "webarebears";

	private static AppDatabase instance;

	public static AppDatabase getAppDatabase(Context context) {
		if (instance == null) {
			instance = Room
					.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DB_NAME)
					.build();
		}
		return instance;
	}

	public abstract UserDao userDao();

}
