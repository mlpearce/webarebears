package uk.co.elysiansoftware.webarebears;

import android.app.Fragment;

/**
 * Activity repesenting the user details form.
 * <p>
 * Created by mlp on 15/12/17.
 */
public class FormActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new FormFragment();
	}
}
