package uk.co.elysiansoftware.webarebears.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Bean class holding the user data.
 * <p>
 * Created by mlp on 15/12/17.
 */
@Entity
public class User {

	@PrimaryKey(autoGenerate = true)
	private Long uid;

	private String name;
	private int age;
	private String emailAddress;
	private String twitterHandle;
	private String pictureFile;

	private int sentUser = 0;
	private int sentAdmin = 0;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTwitterHandle() {
		return twitterHandle;
	}

	public void setTwitterHandle(String twitterHandle) {
		this.twitterHandle = twitterHandle;
	}

	public String getPictureFile() {
		return pictureFile;
	}

	public void setPictureFile(String pictureFile) {
		this.pictureFile = pictureFile;
	}

	public void setSentUser(boolean sentUser) {
		this.sentUser = (sentUser ? 1 : 0);
	}

	public void setSentAdmin(boolean sentAdmin) {
		this.sentAdmin = (sentAdmin ? 1 : 0);
	}

	public int getSentUser() {
		return sentUser;
	}

	public void setSentUser(int sentUser) {
		this.sentUser = sentUser;
	}

	public int getSentAdmin() {
		return sentAdmin;
	}

	public void setSentAdmin(int sentAdmin) {
		this.sentAdmin = sentAdmin;
	}
}
