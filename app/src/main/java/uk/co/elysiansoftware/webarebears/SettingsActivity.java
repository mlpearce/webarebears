package uk.co.elysiansoftware.webarebears;

import android.app.Fragment;

/**
 * Activity to manage the preferences.
 * <p>
 * Created by mlp on 05/01/18.
 */
public class SettingsActivity extends SingleFragmentActivity {

	public static final String SMTP_ADDRESS_PREF = "pref_smtp_server";
	public static final String SMTP_PORT_PREF = "pref_smtp_port";
	public static final String SMTP_USER_PREF = "pref_smtp_user";
	public static final String SMTP_PASSWORD_PREF = "pref_smtp_password";
	public static final String FROM_ADDRESS_PREF = "pref_from_address";

	protected Fragment createFragment() {
		return new SettingsFragment();
	}

}
